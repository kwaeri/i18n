# Authors

This file lists the authors of - and contributors to - this project.

## The List

| From |      |
|------|------|
| 2022 | [Richard Winters](mailto:kirvedx@gmail.com) |