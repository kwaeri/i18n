/**
 * SPDX-PackageName: kwaeri/i18n
 * SPDX-PackageVersion: 0.5.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


 'use strict'


// INCLUDES
import {
    kwaeri
} from './src/i18n.mjs';

const i18n = kwaeri.Internationalization.i18n,
      Internationalization = kwaeri.Internationalization;


// ESM WRAPPER
export {
    i18n
};
