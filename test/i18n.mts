/**
 * SPDX-PackageName: kwaeri/i18n
 * SPDX-PackageVersion: 0.5.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


 'use strict'

// INCLUDES
import * as assert from 'assert';
import * as fs from 'fs/promises';
import * as path from 'path';
import { kwaeri } from '../src/i18n.mjs';
import debug from 'debug';


// DEFINES

const DEBUG = debug( 'kwaeri:i18n-tests' );



// SANITY CHECK - Makes sure our tests are working proerly
describe(
    'PREREQUISITE',
    () => {

        describe(
            'Sanity Test(s)',
            () => {

                it(
                    'Should return true.',
                    async () => {
                        //const version = JSON.parse( ( await fs.readFile( path.join( './', 'package.json' ), { encoding: "utf8" } ) ) ).version;

                        //console.log( `VERSION: ${version}` );

                        return Promise.resolve(
                            assert.equal( [1,2,3,4].indexOf(4), 3 )
                        );
                    }
                );

            }
        );

    }
);


// Primary tests for the module
describe(
    'i18n Functionality Tests',
    () => {

        describe(
            'Load Locales Test I [Default only]',
            () => {

                it(
                    'Should return true, indicating that the default locale loaded - if exists.',
                    async () => {

                        const i18n = new kwaeri.Internationalization.i18n();
                        const result = await i18n.init();

                        DEBUG( result );

                        return Promise.resolve(
                            assert.equal(
                                JSON.stringify( { "en": true } ),
                                JSON.stringify( result )
                            )
                        );
                    }
                );

            }
        );

        describe(
            'Load Locales Test II [Provided and Default]',
            () => {

                it(
                    'Should return true, indicating that the default locale loaded - if exists.',
                    async () => {

                        const i18n = new kwaeri.Internationalization.i18n( "en_US", "en_GB", "es_ES", "de_DE" );
                        const result = await i18n.init();

                        DEBUG( result );

                        return Promise.resolve(
                            assert.equal(
                                JSON.stringify( { "en": true, "en_US": true, "en_GB": true, "es_ES": true, "de_DE": true } ),
                                JSON.stringify( result )
                            )
                        );
                    }
                );

            }
        );

        describe(
            'Get Translated Message Test I [Placeholder(s) Defined, NOT Provided]',
            () => {

                it(
                    'Should return the localised string.',
                    async () => {

                        const i18n = new kwaeri.Internationalization.i18n();
                        const result = await i18n.init();
                        const error = i18n.getMessage( "error" );

                        DEBUG( result );

                        return Promise.resolve(
                            assert.equal(
                                error,
                                "Error: $details$"
                            )
                        );
                    }
                );

            }
        );

        describe(
            'Get Translated Message Test II [Placeholder(s) Defined, Provided]',
            () => {

                it(
                    'Should return the localised string.',
                    async () => {

                        const i18n = new kwaeri.Internationalization.i18n();
                        const result = await i18n.init();
                        const error = i18n.getMessage( "error", "Testing the i18n placeholder functionality.", "Next" );

                        DEBUG( result );

                        return Promise.resolve(
                            assert.equal(
                                error,
                                "Error: Testing the i18n placeholder functionality."
                            )
                        );
                    }
                );

            }
        );

        describe(
            'Get Specific Translated Message Test I [Placeholder(s) Defined, NOT Provided]',
            () => {

                it(
                    'Should return the localised string for specified locale.',
                    async () => {

                        const i18n = new kwaeri.Internationalization.i18n( "de_DE" );
                        const result = await i18n.init();
                        const error = i18n.getLocalisedMessage(  "error", "de_DE" );

                        DEBUG( result );

                        return Promise.resolve(
                            assert.equal(
                                error,
                                "Fehler: $details$"
                            )
                        );
                    }
                );

            }
        );

        describe(
            'Get Specific Translated Message Test II [Placeholder(s) Defined, Provided]',
            () => {

                it(
                    'Should return the localised string for specified locale.',
                    async () => {

                        const i18n = new kwaeri.Internationalization.i18n( "de_DE" );
                        const result = await i18n.init();
                        const error = i18n.getLocalisedMessage( "error", "de_DE", "Testen der i18n-Platzhalterfunktionalität.", "Nächste" );

                        DEBUG( result );

                        return Promise.resolve(
                            assert.equal(
                                error,
                                "Fehler: Testen der i18n-Platzhalterfunktionalität."
                            )
                        );
                    }
                );

            }
        );


    }
);